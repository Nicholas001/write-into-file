import java.io.Serializable;

public class Car  implements Serializable{

    private int id;
    private String marca;
    private String model;
    private int anulFabricatie;

    public Car() {
    }

    public Car(int id, String marca, String model, int anulFabricatie) {
        this.id = id;
        this.marca = marca;
        this.model = model;
        this.anulFabricatie = anulFabricatie;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getMarca() {
        return marca;
    }

    public void setMarca(String marca) {
        this.marca = marca;
    }

    public String getModel() {
        return model;
    }

    public void setModel(String model) {
        this.model = model;
    }

    public int getAnulFabricatie() {
        return anulFabricatie;
    }

    public void setAnulFabricatie(int anulFabricatie) {
        this.anulFabricatie = anulFabricatie;
    }
}

import com.fasterxml.jackson.databind.ObjectMapper;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class Main {

    public static void main(String[] args) {

        List<Car> cars = new ArrayList<Car>();

        Car car1 = new Car(1, "Skoda", "Octavia", 2007);
        Car car2 = new Car(2, "Audi", "A6", 2010);
        Car car3 = new Car(3, "VW", "Golf", 2013);

        cars.add(car1);
        cars.add(car2);
        cars.add(car3);

        writeObjectIntoFile(cars);
    }

    public static void writeObjectIntoFile(List<Car> cars) {
        ObjectMapper mapper = new ObjectMapper();
        File file = new File("C:\\Users\\danal\\IdeaProjects\\SpringAlexProject\\FileWriteHomework\\src\\main\\resources\\cars.json");
        try {
            mapper.writeValue(file, cars);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }


}

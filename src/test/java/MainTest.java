import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.Assert;
import org.junit.Test;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class MainTest {

    @Test
    public void testWriteObjectIntoFile() {
        List<Car> cars = new ArrayList<Car>();
        Car car1 = new Car(1, "Opel", "Agila", 2001);
        cars.add(car1);
        Main.writeObjectIntoFile(cars);
        File file = new File("C:\\Users\\danal\\IdeaProjects\\SpringAlexProject\\FileWriteHomework\\src\\main\\resources\\cars.json");
        ObjectMapper mapper = new ObjectMapper();
        List<Car> newList;
        Car newCar;
        try {
            newList = Arrays.asList(mapper.readValue(file, Car[].class));
            newCar = newList.get(0);
            Assert.assertEquals("Opel", newCar.getMarca());
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
